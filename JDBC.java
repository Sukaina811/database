import java.sql.*;

public class Assignment2 {
    
    // A connection to the database
    Connection connection;
    
    // Statement to run queries
    Statement sql;
    
    // Prepared Statement
    PreparedStatement ps;
    
    // Resultset for the query
    ResultSet rs;
    
    //CONSTRUCTOR
    public Assignment2(){
        //Identified the postgreSQL driver using Class.forName method
        try {
            // Load JDBC driver
            //System.out.println("before");
            Class.forName("org.postgresql.Driver");
            //System.out.println("after");
            
        } catch (ClassNotFoundException e) {
            //System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
            e.printStackTrace();
            return;
        }
        //System.out.println("PostgreSQL JDBC Driver Registered!");
    }
    
    //Using the input parameters, establish a connection to be used for this session. Returns true if connection is sucessful
    public boolean connectDB(String URL, String username, String password){
        try {
            connection = DriverManager.getConnection(URL, username, password);
        } catch (SQLException e) {
            //System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
        if (connection != null) {
            //System.out.println("You made it, take control of your database now!");
            return true;
        } else {
            //System.out.println("Failed to make connection!");
            return false;
        }
    }
    
    //Closes the connection. Returns true if closure was successful
    public boolean disconnectDB(){
        try{
            connection.close();
            //System.out.println("You've been disconnected");
            return true;
            
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean insertPlayer(int pid, String pname, int globalRank, int cid) {
        //check whether insert was successful or not
        String sqlText;
        String sqlTxt;
        
        try{
            sql = connection.createStatement();
            sqlText = String.format("SELECT pid FROM player WHERE pid = %1$d", pid);
            rs = sql.executeQuery(sqlText);
            if (rs == null || !rs.next()) {
                sqlText = "INSERT INTO player " +
                "VALUES (?,?,?,?)";
                PreparedStatement p = connection.prepareStatement(sqlText);
                //Populate the prepared statement
                p.setInt(1,pid);
                p.setString(2,pname);
                p.setInt(3,globalRank);
                p.setInt(4,cid);
                //Execute insert
                p.executeUpdate();
                p.close();
                
            }
            
            else{
                //System.out.println("player existssssss");
                return false;
            }
            
            rs.close();
            return true;
        }
        
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return false;
        }
        
    }
    
    public int getChampions(int pid) {
        String sqlText;
        try{
            sql = connection.createStatement();
            sqlText = String.format("SELECT count(pid) FROM champion WHERE pid = %1$d GROUP BY pid", pid);
            rs = sql.executeQuery(sqlText);
            if (rs != null) {
                while (rs.next()){
                    //System.out.println(rs.getInt("count"));
                    return rs.getInt("count");
                }
            }
            else {
                return 0;
            }
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return 0;
        }
        return 0;
    }
    
    public String getCourtInfo(int courtid){
        String sqlText;
        String a = "";
        try {
            sql = connection.createStatement();
            sqlText = String.format("SELECT courtid, courtname, capacity, tname  FROM court, tournament WHERE courtid = %1$d and court.tid = tournament.tid", courtid);
            rs = sql.executeQuery(sqlText);
            
            if (rs != null){
                while (rs.next()) {
                    a = a + rs.getString(1) + ":" + rs.getString(2)+ ":" + rs.getString(3) + ":" + rs.getString(4) + "\n";
                }
            }
            else {
                return "";
            }
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return "";
            
        }
        //	    	System.out.println(a);
        return a;
    }
    
    public boolean chgRecord(int pid, int year, int wins, int losses){
        String sqlText;
        String text;
        try {
            sql = connection.createStatement();
            text = String.format("SELECT pid, year from record where pid = %1$d and year = %2$d", pid, year);
            rs = sql.executeQuery(text);
            if (rs != null)
            {
                while (rs.next()){
                    //System.out.println("changed");
                    sqlText = String.format("UPDATE record SET wins = %1$d, losses = %2$d where pid = %3$d and year = %4$d", wins, losses, pid, year);
                    sql.executeUpdate(sqlText);
                    return true;
                }
                //System.out.println("didn't changed");
                return false;
            }
            else {
                System.out.println("DNE");
                return false;
            }
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return false;
        }
        
        
    }
    
    public boolean deleteMatcBetween(int p1id, int p2id){
        String sqlT;
        String sqlP;
        try{
            sql = connection.createStatement();
            sqlT = String.format("DELETE FROM event where winid = %1$d and lossid = %2$d", p1id, p2id);
            sql.executeUpdate(sqlT);
            sqlP = String.format("DELETE FROM event where winid = %1$d and lossid = %2$d", p2id, p1id);
            sql.executeUpdate(sqlP);
            
            return true;
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return false;
        }
        
    }
    
    
    public String listPlayerRanking(){
        String a = "";
        try{
            sql = connection.createStatement();
            String sqlText = "SELECT pname, globalrank FROM player ORDER BY globalrank asc";
            rs = sql.executeQuery(sqlText);
            while (rs.next()) {
                a = a + rs.getString(1) + ":" + rs.getString(2) + "\n";
                
            }
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return "";
        }
        //	System.out.println(a);
        return a;
        
        
    }
    
    
    public int findTriCircle(){
        try {
            sql = connection.createStatement();
            String sqlText = "SELECT count(foo.a) from (select distinct e1.winid as a, e1.lossid as b, e2.winid as b, e2.lossid as d, e3.winid as e, e3.lossid as f from event e1, event e2, event e3 where e1.winid <> e2.winid and e1.winid <> e2.lossid and e2.winid <> e3.winid and e2.winid <> e3.lossid and e1.winid = e3.lossid and e1.lossid = e2.winid and e2.lossid = e3.winid) as foo";
            rs = sql.executeQuery(sqlText);
            if (rs != null){
                while (rs.next()){
                    //			System.out.println(rs.getInt("count"));
                    return rs.getInt("count");
                }
            }
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return 0;
        }
        return 0;
    }
    
    
    public boolean updateDB(){
        String sqlText1;
        String sqlText;
        String sqlText2;
        try{
            sql = connection.createStatement();
            sqlText1 = "DROP TABLE IF EXISTS championPlayers CASCADE";
            sql.executeUpdate(sqlText1);
            sqlText = "CREATE TABLE championPlayers(" +
            "pid         INTEGER     PRIMARY KEY," +
            "pname       VARCHAR     NOT NULL," +
            "nchampions  INTEGER	NOT NULL)";
            sql.executeUpdate(sqlText);
            sqlText2 = "INSERT INTO championPlayers(select distinct champion.pid, player.pname, count(champion.pid) FROM champion, player where champion.pid = player.pid group by champion.pid, player.pid)";
            sql.executeUpdate(sqlText2);
            return true;
        }
        catch (SQLException e){
            System.out.println("Query Exection Failed!");
            e.printStackTrace();
            return false;
        }
        //CHECK query in SQL
        //CHECK IF update was successful or not 
    }
    /*
     public static void main(String[] args) {
     // TODO Auto-generated method stub
     Assignment2 a2 = new Assignment2();
     a2.connectDB("jdbc:postgresql://localhost:5432/rizvisuk","rizvisuk","password");
     //		a2.disconnectDB();
     a2.insertPlayer(137,"bb",66,3);
     a2.insertPlayer(126,"B.Moore",22,2);
     a2.getChampions(123);
     a2.getCourtInfo(2);
     a2.chgRecord(123,2011,4,4);
     a2.deleteMatcBetween(125,126);
     a2.listPlayerRanking();
     a2.updateDB();
     a2.findTriCircle();
     }
     */
    
    
    
}